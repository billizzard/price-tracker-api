# Price-tracker-api

Check products prices

### Installation

Clone repository
```sh
git clone git@bitbucket.org:billizzard/price-tracker-api.git
```

Go to the folder
```bash
cd price-tracker-api
```

Run follow commands
```bash
cp .env.dist .env
```

### Run docker
```bash
docker-compose build
docker-compose up -d
docker-compose exec php bash
```
Add permission for var folder
```bash
chmod -R 777 var

```

### Generate the SSH keys for JWT

```bash
cd backend

mkdir config/jwt
sudo chmod -R 777 config/jwt

openssl genrsa -out config/jwt/private.pem -aes256 4096
openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem
openssl rsa -in config/jwt/private.pem -out config/jwt/private2.pem

mv config/jwt/private.pem config/jwt/private.pem-back
mv config/jwt/private2.pem config/jwt/private.pem
rm config/jwt/private.pem-back
```

### Install deps and Generate DB schema

```bash
docker-compose exec php composer install
# drop database
docker-compose exec php php bin/console d:d:d --force
# create database
docker-compose exec php php bin/console d:d:c
# create structure with migrations
docker-compose exec php php bin/console d:m:m
# add fixtures
docker-compose exec php php bin/console d:f:l
```


