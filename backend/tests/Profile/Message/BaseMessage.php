<?php
namespace App\Tests\Profile\Message;

use App\Entity\Message;
use App\Repository\MessageRepository;
use App\Tests\Base;

class BaseMessage extends Base
{
    /** @var MessageRepository */
    public static $messageRepository;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        $entityManager = self::getEntityManager();
        if ($entityManager) {
            static::$messageRepository = $entityManager->getRepository(Message::class);
            static::removeTestData();
        }
    }

    public function createTestMessages()
    {
        $client = self::createAuthenticatedClient();
        $user[] = static::getUserByEmail(static::$user1Email);
        $user[] = static::getUserByEmail(static::$user2Email);
        $entityManager = self::getEntityManager();

        foreach($this->getMessagesData() as $key => $value) {
            $message = new Message();
            $message->setMessage($value[0]);
            $message->setTitle($value[1]);
            $message->setUser($user[$key]);
            $message->setType($value[3]);

            $entityManager->persist($message);

        }
        $entityManager->flush();
    }

    protected function getMessagesData()
    {
        return [
            0 => ['Message 1', 'Title 1', '', Message::TYPE_ACHIEVED_DESIRED_PRICE],
            1 => ['Message 2', 'Title 2', '', Message::TYPE_ACHIEVED_DESIRED_PRICE],
        ];
    }

    protected static function removeTestData(): void
    {
        $entityManager = static::getEntityManager();
        $user = static::getUserByEmail(static::$user1Email);
        $user2 = static::getUserByEmail(static::$user2Email);

        if ($user) {
            if ($messages = $user->getMessages()) {
                foreach ($messages as $message) {
                    $entityManager->remove($message);
                }
            }
        }

        if ($user2) {
            if ($messages = $user2->getMessages()) {
                foreach ($messages as $message) {
                    $entityManager->remove($message);
                }
            }
        }

        $entityManager->flush();
    }
}
