<?php
namespace App\Tests\Profile\Message;

class MessageDeleteTest extends BaseMessage
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    public function testCreateTestData(): array
    {
        $this->createTestMessages();
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'GET', '/api/profile/messages');
        $user1Messages = json_decode($client->getResponse()->getContent());
        $this->assertCount(1, $user1Messages);
        return $user1Messages;
    }

    /**
     * @depends testCreateTestData
     */
    public function testDeleteNotMyTest(array $messages): void
    {
        $client = self::createAuthenticatedClient(static::$user2Email);

        $this->authRequest($client,'DELETE', '/api/profile/messages/' . $messages[0]->id);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * @depends testCreateTestData
     */
    public function testDeleteMessage(array $messages): array
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'DELETE', '/api/profile/messages/' . $messages[0]->id);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
        return $messages;
    }

    /**
     * @depends testDeleteMessage
     */
    public function testMessageCountAfterDelete(): void
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'GET', '/api/profile/messages');

        $messages = json_decode($client->getResponse()->getContent());
        $this->assertCount(0, $messages);
    }

    /**
     * @depends testDeleteMessage
     */
    public function testDeleteMessageAfterDelete(array $messages): void
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'DELETE', '/api/profile/messages/' . $messages[0]->id);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}
