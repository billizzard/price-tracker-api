<?php
namespace App\Tests\Profile\Message;

class MessageGetTest extends BaseMessage
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    public function testGetMessage(): void
    {
        $this->createTestMessages();
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'GET', '/api/profile/messages');
        $user1Messages = json_decode($client->getResponse()->getContent());
        $this->assertCount(1, $user1Messages);
        $this->assertEquals('Title 1', $user1Messages[0]->title);
    }
}
