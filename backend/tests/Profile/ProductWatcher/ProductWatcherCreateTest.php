<?php
namespace App\Tests\Profile\ProductWatcher;

class ProductWatcherCreateTest extends BaseProductWatcher
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        self::removeTestDataForSecondUser();
    }

    public function testCreateTestData(): void
    {
        $client = $this->createTestProductWatcher();
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testCreateProductWatcherForSecondTestUser(): void
    {
        $client = self::createAuthenticatedClient(static::$user2Email);
        $this->authRequest($client, 'POST', '/api/profile/product_watchers', [
            'url' => static::$product1Url,
            'startPrice' => '16',
            'title' => 'ProductForUser2',
            'percent' => 20
        ]);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    /**
     * @depends testCreateTestData
     */
    public function testCountItemsAfterCreate()
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'GET', '/api/profile/product_watchers');

        $productWatchers = json_decode($client->getResponse()->getContent());
        $this->assertCount(2, $productWatchers);
        return $productWatchers;
    }

    public function testCreateExistingProductWatcher()
    {
        $client = self::createAuthenticatedClient();
        $this->authRequest($client,'POST', '/api/profile/product_watchers', [
            'url' => self::$product1Url,
            'currentPrice' => '15',
            'title' => 'Product1',
            'percent' => '10'
        ]);

        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    protected static function removeTestDataForSecondUser(): void
    {
        $entityManager = static::getEntityManager();
        $product1 = static::$productRepository->findOneByUrl(static::$product1Url);
        $user = static::getUserByEmail(static::$user2Email);

        if ($user) {
            if ($product1) {
                $productWatcher = static::$productWatcherRepository->findOneByUserAndProduct($user->getId(), $product1->getId());
                if ($productWatcher) {
                    $entityManager->remove($productWatcher);
                }
            }

            $entityManager->flush();
        }
    }
}
