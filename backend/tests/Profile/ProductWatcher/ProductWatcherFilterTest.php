<?php
namespace App\Tests\Profile\ProductWatcher;

use App\Entity\Product;
use App\Entity\ProductWatcher;
use App\Repository\ProductRepository;
use App\Repository\ProductWatcherRepository;
use App\Tests\Base;

class ProductWatcherFilterTest extends BaseProductWatcher
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    public function testCreateTestData(): void
    {
        $client = $this->createTestProductWatcher();
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    /**
     * @depends testCreateTestData
     */
    public function testCountItemsAfterCreate()
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'GET', '/api/profile/product_watchers');

        $productWatchers = json_decode($client->getResponse()->getContent());
        $this->assertCount(2, $productWatchers);
        return $productWatchers;
    }

    /**
     * @depends testCountItemsAfterCreate
     */
    public function testTitleFilter(): void
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'GET', '/api/profile/product_watchers', [], ['title' => 'Product2']);

        $productWatchers = json_decode($client->getResponse()->getContent());
        $this->assertCount(1, $productWatchers);
    }

    /**
     * @depends testCountItemsAfterCreate
     */
    public function testTitleFilterNotFound(): void
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'GET', '/api/profile/product_watchers', [], ['title' => 'Product33']);

        $productWatchers = json_decode($client->getResponse()->getContent());
        $this->assertCount(0, $productWatchers);
    }

    /**
     * @depends testCountItemsAfterCreate
     */
    public function testTitleOrderFilter(): void
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'GET', '/api/profile/product_watchers', [], ['order[createdAt]' => 'desc']);

        $productWatchers = json_decode($client->getResponse()->getContent());
        if ($productWatchers[0]->createdAt !== $productWatchers[1]->createdAt) {
            $this->assertEquals('Product2', $productWatchers[0]->title);
        } else {
            $this->assertTrue(true);
        }
    }
}
