<?php
namespace App\Tests\Profile\ProductWatcher;

class ProductWatcherDeleteTest extends BaseProductWatcher
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    public function testCreateTestData(): void
    {
        $client = $this->createTestProductWatcher();
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    /**
     * @depends testCreateTestData
     */
    public function testCountItemsAfterCreate()
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'GET', '/api/profile/product_watchers');

        $productWatchers = json_decode($client->getResponse()->getContent());
        $this->assertCount(2, $productWatchers);
        return $productWatchers;
    }

    /**
     * @depends testCountItemsAfterCreate
     */
    public function testDeleteExistingProductWatcher($productWatchers)
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'DELETE', '/api/profile/product_watchers/' . $productWatchers[0]->id);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }

    /**
     * @depends testDeleteExistingProductWatcher
     */
    public function testCountItemsAfterDelete()
    {
        $client = self::createAuthenticatedClient();

        $this->authRequest($client,'GET', '/api/profile/product_watchers');

        $productWatchers = json_decode($client->getResponse()->getContent());
        $this->assertCount(1, $productWatchers);
    }
}
