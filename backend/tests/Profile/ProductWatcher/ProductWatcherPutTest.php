<?php
namespace App\Tests\Profile\ProductWatcher;

class ProductWatcherPutTest extends BaseProductWatcher
{
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
    }

    public function testCreateTestData(): void
    {
        $client = $this->createTestProductWatcher();
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    public function testCreateProductWatcherForSecondTestUser(): void
    {
        $client = self::createAuthenticatedClient();
        $product = static::$productRepository->findOneByUrl(static::$product1Url);
        $user = static::getUserByEmail(static::$user1Email);
        $productWatcher = static::$productWatcherRepository->findOneByUserAndProduct($user->getId(), $product->getId());

        $this->authRequest($client, 'PUT', '/api/profile/product_watchers/' . $productWatcher->getId(), [
            'url' => static::$product1Url,
            'startPrice' => '444',
            'title' => 'PUT Product1',
            'percent' => 44
        ]);

        $productWatcher = json_decode($client->getResponse()->getContent());

        $this->assertEquals(44, $productWatcher->percent);
        $this->assertEquals('Product1', $productWatcher->title);
        $this->assertEquals('15.22', $productWatcher->startPrice);
    }
}
