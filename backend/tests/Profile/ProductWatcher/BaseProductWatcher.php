<?php
namespace App\Tests\Profile\ProductWatcher;

use App\Entity\Product;
use App\Entity\ProductWatcher;
use App\Repository\ProductRepository;
use App\Repository\ProductWatcherRepository;
use App\Tests\Base;

class BaseProductWatcher extends Base
{
    /** @var ProductWatcherRepository */
    public static $productWatcherRepository;
    /** @var ProductRepository */
    public static $productRepository;
    public static $product1Url = 'http://test.by/test1';
    public static $product2Url = 'http://test.by/test2';

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        $entityManager = self::getEntityManager();
        if ($entityManager) {
            static::$productWatcherRepository = $entityManager->getRepository(ProductWatcher::class);
            static::$productRepository = $entityManager->getRepository(Product::class);
            static::removeTestData();
        }
    }

    public function createTestProductWatcher()
    {
        $client = self::createAuthenticatedClient();
        foreach($this->productWatchersData() as $value) {
            $this->authRequest($client, 'POST', '/api/profile/product_watchers', [
                'url' => $value[0],
                'startPrice' => $value[1],
                'title' => $value[2],
                'percent' => $value[3]
            ]);
        }

        return $client;
    }

    protected function productWatchersData(): array
    {
        return [
            'First Product' => [self::$product1Url, '15.22', 'Product1', 20],
            'Second Product' => [self::$product2Url, '10', 'Product2', 10],
        ];
    }

    protected static function removeTestData(): void
    {
        $entityManager = static::getEntityManager();
        $product1 = static::$productRepository->findOneByUrl(static::$product1Url);
        $product2 = static::$productRepository->findOneByUrl(static::$product2Url);
        $user = static::getUserByEmail(static::$user1Email);

        if ($user) {
            if ($product1) {
                $productWatcher = static::$productWatcherRepository->findOneByUserAndProduct($user->getId(), $product1->getId());
                if ($productWatcher) {
                    $entityManager->remove($productWatcher);
                }
            }

            if ($product2) {
                $productWatcher = static::$productWatcherRepository->findOneByUserAndProduct($user->getId(), $product2->getId());
                if ($productWatcher) {
                    $entityManager->remove($productWatcher);
                }
            }

            $entityManager->flush();
        }
    }
}
