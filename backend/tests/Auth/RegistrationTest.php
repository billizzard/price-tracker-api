<?php
namespace App\Tests\Auth;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Tests\Base;

class RegistrationTest extends Base
{
    private static $entityManager;
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        self::$entityManager = self::getEntityManager();
        /** @var UserRepository $userRepository */
        $userRepository = static::$entityManager->getRepository(User::class);
        $user = $userRepository->findOneByEmail('test@test.test');
        if ($user) {
            static::$entityManager->remove($user);
            static::$entityManager->flush();
        }
    }

    public function testRegistrationSuccess(): void
    {
        static::createUser('test@test.test', 'aaaaaa');

        $this->assertEquals(200, self::$client->getResponse()->getStatusCode());
    }

    /**
     * @depends testRegistrationSuccess
     */
    public function testRegistrationAlready(): void
    {
        static::$client->request('POST','/api/auth/registration', [], [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'email' => 'test@test.test',
                'password' => 'aaaaaa',
                'confirm_password' => 'aaaaaa',
            ])
        );

        $this->assertContains('Email', static::$client->getResponse()->getContent());
        $this->assertEquals(401, static::$client->getResponse()->getStatusCode());
    }

    /**
     * @depends testRegistrationSuccess
     */
    public function testLoginSuccess(): void
    {
        $this->login('test@test.test', 'aaaaaa');

        $response = json_decode(static::$client->getResponse()->getContent());
        $tokenLength = strlen($response->token);

        $this->assertGreaterThan(0, $tokenLength);
    }

    public function testLoginBadEmail(): void
    {
        $this->login('test1@test.test', 'aaaaaa');
        $this->assertEquals(401, static::$client->getResponse()->getStatusCode());
    }

    public function testLoginBadPassword(): void
    {
        $this->login('test@test.test', 'aaaaaab');
        $this->assertEquals(401, static::$client->getResponse()->getStatusCode());
    }

    private function login($userName, $password): void
    {
        self::$client->request('POST','/api/auth/login', [], [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'username' => $userName,
                'password' => $password
            ], true)
        );
    }
}