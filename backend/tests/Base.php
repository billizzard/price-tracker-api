<?php
namespace App\Tests;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class Base extends WebTestCase
{
    protected $token = null;
    protected static $client;
    protected static $user1Email = 'testuser@test.test';
    protected static $user2Email = 'testuser2@test.test';

    public static function setUpBeforeClass()
    {
        self::$kernel = self::bootKernel();
        self::$client = static::createClient();
        self::createUser(self::$user1Email, 'aaaaaa');
        self::createUser(self::$user2Email, 'aaaaaa');
    }

    protected static function createAuthenticatedClient($username = '', $password = 'aaaaaa')
    {
        $username = $username ?: self::$user1Email;

        self::$client->request('POST','/api/auth/login', [], [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'username' => $username,
                'password' => $password
            ])
        );

        $data = json_decode(self::$client->getResponse()->getContent());

        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data->token));

        return $client;
    }

    protected function authRequest($client, $method, $url, $data = [], $parameters = [])
    {
        $client->request($method, $url . '.json', $parameters, [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );
    }

    protected static function createUser($email, $password): void
    {
        $userData = [
            'email' => $email,
            'password' => $password,
            'confirm_password' => $password,
        ];

        static::$client->request('POST','/api/auth/registration', [], [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($userData)
        );
    }

    protected static function getUserByEmail(string $email): ?User
    {
        if ($email) {
            $entityManager = self::getEntityManager();
            if ($entityManager) {
                /** @var UserRepository $userRepository */
                $userRepository = $entityManager->getRepository(User::class);
                if ($userRepository) {
                    $user = $userRepository->findOneByEmail($email);
                    if ($user) {
                        return $user;
                    }
                }
            }
        }

        return null;
    }

    protected static function getEntityManager()
    {
        if (static::$kernel->getContainer()) {
            return static::$kernel->getContainer()->get('doctrine')->getManager();
        }

        return null;
    }
}