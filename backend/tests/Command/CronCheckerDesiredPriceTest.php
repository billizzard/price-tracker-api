<?php
namespace App\Tests\Profile\Message;

use App\Entity\Price;
use App\Entity\Product;
use App\Entity\ProductWatcher;
use App\Tests\Command\BaseCommand;

class CronCheckerDesiredPriceTest extends BaseCommand
{
    private static $entityManager;

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        self::$entityManager = self::getEntityManager();
    }

    public function testCheckPrice(): \stdClass
    {
        $productWatcherJson = $this->createProductWatcher();
        $this->runCheckPrice($productWatcherJson);

        $this->assertEquals(true, true);
        return $productWatcherJson;
    }

    /**
     * @depends testCheckPrice
     */
    public function testProductLastTrackedDate($productWatcherJson): void
    {
        /** @var Product $product */
        $product = self::$entityManager->getRepository(Product::class)->find($productWatcherJson->product->id);
        $this->assertEquals(true, $product->getLastTrackedDate() > 0);
        $this->assertEquals(Product::STATUS_PARSE_SUCCESSFULLY, $product->getStatus());
    }

    /**
     * @depends testCheckPrice
     */
    public function testProductStatus($productWatcherJson): void
    {
        /** @var ProductWatcher $productWatcher */
        $productWatcher = self::$entityManager->getRepository(ProductWatcher::class)->find($productWatcherJson->id);
        $this->assertEquals(true, $productWatcher->getSuccessDate() > 0);
        $this->assertEquals(ProductWatcher::STATUS_NEW_SUCCESS, $productWatcher->getStatus());
    }

    /**
     * @depends testCheckPrice
     */
    public function testPrice($productWatcherJson): void
    {
        $price = self::$entityManager->getRepository(Price::class)->findBy(['product' => $productWatcherJson->product->id]);
        $this->assertCount(1, $price);
    }
}
