<?php
namespace App\Tests\Command;

use App\Entity\Price;
use App\Entity\Product;
use App\Entity\ProductWatcher;
use App\Parsers\PriceParserFactory;
use App\Parsers\PriceParsers\PriceParser;
use App\Repository\ProductRepository;
use App\Repository\ProductWatcherRepository;
use App\Service\PriceChecker\PriceChecker;
use App\Tests\Base;

class BaseCommand extends Base
{
    /** @var ProductWatcherRepository */
    public static $productWatcherRepo;
    /** @var ProductRepository */
    public static $productRepo;
    private static $productWatcherTitle = 'CronCheckPriceProductTest';
    private static $productWatcherUrl = 'https://localhost/test';

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();
        $entityManager = self::getEntityManager();
        if ($entityManager) {
            static::$productWatcherRepo = $entityManager->getRepository(ProductWatcher::class);
            static::$productRepo = $entityManager->getRepository(Product::class);
            static::removeTestData();
        }
    }

    public function createProductWatcher($startPrice = 100): \stdClass
    {
        $client = self::createAuthenticatedClient();
        $this->authRequest($client, 'POST', '/api/profile/product_watchers', [
            'url' => self::$productWatcherUrl,
            'startPrice' => (string) $startPrice,
            'title' => self::$productWatcherTitle,
            'percent' => 10
        ]);

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
        return json_decode($client->getResponse()->getContent());
    }

    protected function runCheckPrice($productWatcherJson)
    {
        /** @var ProductWatcher $productWatcher */
        $entityManager = self::getEntityManager();
        $product = $entityManager->getRepository(Product::class)->find($productWatcherJson->product->id);
        $factory = $this->getMockPriceParserFactory();

        $priceChecker = new PriceChecker($entityManager, $factory);
        $priceChecker->checkPrices([$product]);
    }

    protected function getMockPriceParserFactory()
    {
        $priceParserMock = $this->getMockBuilder(PriceParser::class)->setMethods(['getPriceByUrl'])->getMock();
        $priceParserMock->method('getPriceByUrl')->willReturn(90);

        $factory = $this->getMockBuilder(PriceParserFactory::class)->setMethods(['createPriceParser'])->getMock();
        $factory->method('createPriceParser')->willReturn($priceParserMock);
        return $factory;
    }

    protected static function removeTestData(): void
    {
        $entityManager = static::getEntityManager();
        $qb = $entityManager->createQueryBuilder();
        $qb->delete(ProductWatcher::class, 'pw');
        $qb->where('pw.title = :title');
        $qb->setParameter('title', self::$productWatcherTitle)->getQuery();
        $qb->getQuery()->execute();
        $entityManager->flush();

        $products = self::$productRepo->findBy(['url' => self::$productWatcherUrl]);

        $ids = array_map(function($product) {
            return $product->getId();
        }, $products);

        if ($ids) {
            $qb = $entityManager->createQueryBuilder();
            $qb->delete(Price::class, 'pr');
            $qb->where('pr.product IN(:ids)');
            $qb->setParameter('ids', implode(',',$ids))->getQuery();
            $qb->getQuery()->execute();
        }

        $qb = $entityManager->createQueryBuilder();
        $qb->delete(Product::class, 'p');
        $qb->where('p.url = :url');
        $qb->setParameter('url', self::$productWatcherUrl)->getQuery();
        $qb->getQuery()->execute();
    }
}
