<?php
namespace App\Service\PriceChecker;

use App\Entity\Message;
use App\Entity\Price;
use App\Entity\Product;
use App\Entity\ProductWatcher;
use App\Parsers\PriceParserFactory;
use App\Parsers\SiteParsers\FileGetContentParser;
use App\Repository\MessageRepository;
use App\Repository\ProductWatcherRepository;
use Doctrine\Common\Persistence\ObjectManager;

class PriceChecker
{
    private $priceParserFactory;
    private $manager;

    public function __construct(
        ObjectManager $manager,
        PriceParserFactory $priceParserFactory
    )
    {
        $this->manager = $manager;
        $this->priceParserFactory = $priceParserFactory;
    }

    public function checkPrices(array $products = []): void
    {
        //$products = $productRepository->findAll();
        //$products = $products ? $products : $this->manager->getRepository(Product::class)->findAll();
        $products = $products ?: $this->manager->getRepository(Product::class)->findBy(['id' => 4]);

        if ($products) {
            foreach ($products as $product) {
                $this->checkProductPrice($product);
            }

            $this->processSuccessfulProductWatchers();
        }
    }

    private function checkProductPrice(Product $product): void
    {
        $price = $this->parseProductPrice($product);
        $this->processPrice($product, $price);
    }

    public function parseProductPrice(Product $product)
    {
        $parser = $this->priceParserFactory->createPriceParser($product->getPriceParser());

        if ($parser) {
            //return (float) 1506;
            return (float) $parser->getPriceByUrl($product->getUrl());
        }

        return false;
    }

//    public function checkProductPrice(Product $product): void
//    {
//        $priceParserClass = $product->getPriceParser();
//        if ($priceParserClass) {
//            $parser = new $priceParserClass($this->siteParser);
//            $price = (float) 1506;
//            //$price = (float) $parser->getPriceByUrl($product->getUrl());
//            if ($price === false) {
//                $product->setStatus(Product::STATUS_PARSE_FAIL);
//            } else {
//                $product->setStatus(Product::STATUS_PARSE_SUCCESSFULLY);
//
//                if ((float) $product->getCurrentPrice() !== $price) {
//                    $this->processChangedPrice($product, $price);
//                }
//            }
//
//            $this->manager->flush();
//        }
//    }

    private function processPrice(Product $product, $price): void
    {
        if ($price === false) {
            $product->setStatus(Product::STATUS_PARSE_FAIL);
        } else {
            $product->setStatus(Product::STATUS_PARSE_SUCCESSFULLY);

            if ((float) $product->getCurrentPrice() !== $price) {
                $this->processChangedPrice($product, $price);
            }
        }

        $this->manager->flush();
    }

    private function processChangedPrice(Product $product, $price): void
    {
        $product->setCurrentPrice($price);
        $priceObj = $this->createPrice($product, $price);
        $this->manager->persist($priceObj);

        /** @var ProductWatcherRepository $productWatcherRepository */
        $productWatcherRepository = $this->manager->getRepository(ProductWatcher::class);
        $productWatcherRepository->updateStatusIfPriceDesired($priceObj, $product);

        $product->setLastTrackedDate(time());
    }

    private function createPrice(Product $product, $price): Price
    {
        $priceObj = new Price();
        $priceObj->setProduct($product);
        $priceObj->setPrice($price);
        $priceObj->setDate(time());
        return $priceObj;
    }

    private function processSuccessfulProductWatchers(): void
    {
        $productWatchers = $this->manager->getRepository(ProductWatcher::class)->getNewSuccessful();
        /** @var MessageRepository $messageRepository */
        $messageRepository = $this->manager->getRepository(Message::class);

        foreach ($productWatchers as $productWatcher) {
            $messageRepository->createSuccessMessageByProductWatcher($productWatcher);
        }
    }
}
