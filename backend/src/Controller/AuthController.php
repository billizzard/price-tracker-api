<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;

class AuthController extends AbstractController
{
    /**
     * @Route(
     *     name="registration",
     *     path="api/auth/registration",
     *     methods={"POST"},
     *     defaults={
     *       "_controller"="\App\Controller\AuthController::registration",
     *       "_api_item_operation_name"="registration"
     *     }
     *   )
     */
    public function registration(Request $request, UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository)
    {
        $data = json_decode($request->getContent(), true);
        if (isset($data['confirm_password']) && $data['confirm_password'] !== $data['password']) {
            return $this->json([
                'message' => 'Translate101: Пароли не совпадают',
            ], 401);
        }

        if (isset($data['email'])) {
            if (strpos($data['email'], '@') === false) {
                return $this->json([
                    'message' => 'Translate101:  Не верный email',
                ], 401);
            }

            $foundUser = $userRepository->findOneByEmail($data['email']);

            if ($foundUser) {
                return $this->json([
                    'message' => 'Translate101:  Email уже занят',
                ], 401);
            }

            $user = new User();
            $user->setPlainPassword($data['password']);
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setEmail($data['email']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->json([
                'id' => $user->getId(),
            ]);
        }
    }
}
