<?php

namespace App\Command;

use App\Service\PriceChecker\PriceChecker;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CronCheckerPriceCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'cron:check-price';
    private $priceChecker;

    public function __construct(PriceChecker $priceChecker)
    {
        $this->priceChecker = $priceChecker;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName(self::$defaultName)->setDescription('Check product price on sites');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->priceChecker->checkPrices();
        $output->writeln('Prices checked!');
    }
}
