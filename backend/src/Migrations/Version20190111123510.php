<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190111123510 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product ADD price_parser_id INT DEFAULT NULL, DROP price_parser');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD76FF76C7 FOREIGN KEY (price_parser_id) REFERENCES price_parser (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD76FF76C7 ON product (price_parser_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD76FF76C7');
        $this->addSql('DROP INDEX IDX_D34A04AD76FF76C7 ON product');
        $this->addSql('ALTER TABLE product ADD price_parser VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, DROP price_parser_id');
    }
}
