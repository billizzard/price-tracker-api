<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductWatcherRepository")
 * @ORM\HasLifecycleCallbacks()
 *
 * @ApiResource(
 *     routePrefix="/profile",
 *     attributes={
 *      "normalization_context"={"groups"={"product-watcher-list"}},
 *     },
 *     collectionOperations={
            "post"={
 *              "denormalization_context"={"groups"={"post-pw"}},
 *          },
 *          "get"
 *     },
 *     itemOperations={
 *         "get",
 *         "delete",
 *         "put"={
 *             "denormalization_context"={"groups"={"put-pw"}},
 *         }
 *     }
 * )
 *
 * @ApiFilter(OrderFilter::class, properties={"createdAt", "title"})
 * @ApiFilter(SearchFilter::class, properties={"title": "partial"})
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 */
class ProductWatcher
{
    const STATUS_TRAKCED = 1; // Product still tracked
    const STATUS_NEW_SUCCESS = 2; // Product has desired price, but emails not sent yet, status not set
    const STATUS_SUCCESS = 3; // Product has desired price, and processed

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("product-watcher-list")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Groups({"product-watcher-list", "post-pw"})
     */
    private $title = '';

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Assert\NotBlank()
     * @Groups({"product-watcher-list", "post-pw"})
     */
    private $startPrice;

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true, options={"default" : 0})
     * @Assert\NotBlank()
     * @Groups("product-watcher-list")
     */
    private $desiredPrice = 0;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Assert\Range(
     *      min = 1,
     *      max = 99,
     *      minMessage = "v.percent.range",
     *      maxMessage = "v.percent.range"
     * )
     * @Groups({"product-watcher-list", "put-pw", "post-pw"})
     */
    private $percent = 0;

    /**
     * @ApiProperty(
     *     attributes={
     *         "swagger_context"={"type"="string"}
     *     }
     * )
     * @Assert\Url
     * @Groups("post-pw")
     */
    private $url = '';

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $createdAt = 0;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $successDate = 0;

    /**
     * @ORM\Column(type="smallint")
     * @Groups("product-watcher-list")
     */
    private $status = self::STATUS_TRAKCED;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="productWatchers")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("product-watcher-list")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="productWatchers")
     * @ORM\JoinColumn(nullable=false)
     * @ApiSubresource
     * @Groups("product-watcher-list")
     */
    private $product;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser($user): void
    {
        $this->user = $user;
    }

    public function getStartPrice(): float
    {
        return $this->startPrice;
    }

    public function setStartPrice(float $startPrice): void
    {
        $this->startPrice = $startPrice;
    }

    public function getDesiredPrice(): float
    {
        return $this->desiredPrice;
    }

    public function setDesiredPrice(float $desiredPrice): void
    {
        $this->desiredPrice = $desiredPrice;
    }

    public function createDesiredPrice(): float
    {
        return $this->getStartPrice() - ($this->getStartPrice() * $this->getPercent() / 100);
    }

    /**
     * @return int
     */
    public function getPercent(): int
    {
        return $this->percent;
    }

    /**
     * @param integer $percent
     */
    public function setPercent($percent): void
    {
        $this->percent = $percent;
    }

    /**
     * @return integer
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @param integer $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return integer
     */
    public function getSuccessDate(): int
    {
        return $this->successDate;
    }

    /**
     * @param integer $successDate
     */
    public function setSuccessDate($successDate): void
    {
        $this->successDate = $successDate;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = time();
    }

    public function getProduct(): Product
    {
        return $this->product;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }
}
