<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 *
 * @ApiResource(
 *     collectionOperations={},
 *     itemOperations={"get"}
 * )
 */
class Product
{
    const STATUS_NEW = 1; // новый продукт, непонятно есть ли у него парсер
    const STATUS_PARSE_FAIL = 2; // парсер не смог пропарсить цену у этого товара
    const STATUS_PARSE_SUCCESSFULLY = 3; // парсер успешно пропарсит товар

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("product-watcher-list")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Host", inversedBy="products")
     * @ORM\JoinColumn(nullable=true)
     */
    private $host;

    /**
     * @ORM\Column(type="string", length=600, unique=true)
     * @Assert\Url(message="v.url.invalid")
     * @Assert\NotBlank()
     */
    private $url;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductWatcher", mappedBy="product")
     */
    private $productWatchers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Price", mappedBy="product")
     */
    private $prices;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Assert\NotBlank()
     * @Groups("product-watcher-list")
     */
    private $currentPrice = 0;

    /**
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $lastTrackedDate = 0;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status = self::STATUS_NEW;

    public function __construct()
    {
        $this->productWatchers = new ArrayCollection();
        $this->prices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHost(): Host
    {
        return $this->host;
    }

    public function setHost(Host $host)
    {
        $this->host = $host;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getCurrentPrice()
    {
        return $this->currentPrice;
    }

    public function setCurrentPrice($currentPrice): self
    {
        $this->currentPrice = $currentPrice;

        return $this;
    }

    public function getLastTrackedDate(): ?int
    {
        return $this->lastTrackedDate;
    }

    public function setLastTrackedDate(int $lastTrackedDate): self
    {
        $this->lastTrackedDate = $lastTrackedDate;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|ProductWatcher[]
     */
    public function getProductWatchers()
    {
        return $this->productWatchers;
    }

    /**
     * @return Collection|Price[]
     */
    public function getPrices()
    {
        return $this->prices;
    }

    public function getPriceParser(): string
    {
        if ($host = $this->getHost()) {
            return $host->getParser();
        }

        return '';
    }
}
