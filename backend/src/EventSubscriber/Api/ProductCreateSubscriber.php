<?php

namespace App\EventSubscriber\Api;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Product;
use App\Entity\Host;
use App\Entity\ProductWatcher;
use App\Repository\HostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\TokenExtractorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

final class ProductCreateSubscriber implements EventSubscriberInterface
{
    private $entityManager;
    private $hostRepository;
    private $jwtEncoder;
    private $tokenExtractor;

    public function __construct(EntityManagerInterface $entityManager, HostRepository $hostRepository, JWTEncoderInterface $jwtEncoder, TokenExtractorInterface $tokenExtractor)
    {
        $this->jwtEncoder = $jwtEncoder;
        $this->entityManager = $entityManager;
        $this->hostRepository = $hostRepository;
        $this->tokenExtractor = $tokenExtractor;
    }

    public static function getSubscribedEvents()
    {
        return [];
//            return [
//                KernelEvents::VIEW => ['createHost', EventPriorities::PRE_WRITE],
//                KernelEvents::VIEW => ['createProductWatcher', EventPriorities::POST_WRITE],
//            ];
    }

    public function createHost(GetResponseForControllerResultEvent $event)
    {
        $product = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$product instanceof Product || Request::METHOD_POST !== $method) {
            return;
        }

        $parsedUrl = parse_url($product->getUrl());
        if (isset($parsedUrl['host'])) {
            $host = $this->hostRepository->findOneByName($parsedUrl['host']);
            if (!$host) {
                $host = $this->hostRepository->createByName($parsedUrl['host']);
            }
            $product->setHost($host);
        }
    }

    public function createProductWatcher(GetResponseForControllerResultEvent $event)
    {

        $token = $this->tokenExtractor->extract($event->getRequest());
        if ($token) {
            $payload = $this->jwtEncoder->decode($token);
            $productWatcher = new ProductWatcher();
            $productWatcher->setPercent();
        }

        throw new AuthenticationException('No token');
    }
}
