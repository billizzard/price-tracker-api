<?php

namespace App\EventSubscriber\Api;

use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\TokenExtractorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class JWTCreateSubscriber implements EventSubscriberInterface
{
    private $jwtEncoder;
    private $tokenExtractor;

    public function __construct(JWTEncoderInterface $jwtEncoder, TokenExtractorInterface $tokenExtractor)
    {
        $this->jwtEncoder = $jwtEncoder;
        $this->tokenExtractor = $tokenExtractor;
    }

    public static function getSubscribedEvents()
    {
        return [
            Events::JWT_CREATED => ['createJWT'],
        ];
    }

    /**
     * While creating token we can add some information to token
     * @param JWTCreatedEvent $event
     */
    public function createJWT(JWTCreatedEvent $event)
    {
        $user = $event->getUser();

        $payload = $event->getData();
        $payload['id'] = $user->getId();

        $event->setData($payload);
    }
}
