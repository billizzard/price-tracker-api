<?php

namespace App\Event;

use App\Entity\Message;
use App\Entity\Product;
use Symfony\Component\EventDispatcher\Event;

class AchieveDesiredPriceEvent extends Event
{
    public const CHANGE_PRICE = 'price.change';

    private $price;
    private $product;
    private $message;

    public function __construct(float $price, Product $product, Message $message)
    {
//        $this->test = $test;
//        $this->user = $user;
//        $this->message = $message;
    }

    /**
     * @return Test
     */
    public function getTest(): Test
    {
        return $this->test;
    }

    /**
     * @return Message
     */
    public function getMessage(): Message
    {
        return $this->message;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
