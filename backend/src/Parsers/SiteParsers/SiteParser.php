<?php
namespace App\Parsers\SiteParsers;

interface SiteParser
{
    public function getContent(string $url);
}