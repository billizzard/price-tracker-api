<?php
namespace App\Parsers;

use App\Parsers\PriceParsers\PriceParser;
use App\Parsers\SiteParsers\FileGetContentParser;

class PriceParserFactory
{
    public function createPriceParser(string $className): PriceParser
    {
        $siteParser = new FileGetContentParser();
        return new $className($siteParser);
    }
}
