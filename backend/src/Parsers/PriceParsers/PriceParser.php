<?php
namespace App\Parsers\PriceParsers;

interface PriceParser
{
    public function getPriceByUrl(string $pageContent);
}