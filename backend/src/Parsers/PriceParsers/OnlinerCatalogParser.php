<?php
namespace App\Parsers\PriceParsers;

use App\Parsers\SiteParsers\SiteParser;
use Symfony\Component\DomCrawler\Crawler;

class OnlinerCatalogParser implements PriceParser
{
    private $parser;

    public function __construct(SiteParser $parser)
    {
        $this->parser = $parser;
    }

    public function getPriceByUrl(string $url)
    {
        if ($content = $this->parser->getContent($url)) {
            $crawler = new Crawler($content);
            $priceNode = $crawler->filter('#product-prices-container .offers-description__link');
            if ($priceNode) {
                if ($text = $priceNode->text()) {
                    $prices = explode('–', trim($text));
                    $price = (float)str_replace(',', '.', $prices[0]);
                    if ($price) {
                        return $price;
                    }
                }
            }
        }

        return false;
    }
}
