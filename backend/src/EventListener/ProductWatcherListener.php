<?php
namespace App\EventListener;

use App\Entity\Product;
use App\Entity\Host;
use App\Entity\ProductWatcher;
use App\Entity\User;
use App\Repository\ProductRepository;
use App\Repository\ProductWatcherRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Repository\HostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\TokenExtractorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProductWatcherListener
{
    private $hostRepository;
    private $productRepository;
    /** @var ProductWatcherRepository $productWatcherRepository */
    private $productWatcherRepository;
    private $requestStack;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;


    public function __construct(
        HostRepository $hostRepository,
        ProductRepository $productRepository,
        ProductWatcherRepository $productWatcherRepository,
        RequestStack $requestStack,
        TokenStorageInterface $tokenStorage
    )
    {
        $this->productRepository = $productRepository;
        $this->productWatcherRepository = $productWatcherRepository;
        $this->hostRepository = $hostRepository;
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
    }

    public function postPersist(LifecycleEventArgs $args)
    {

    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof ProductWatcher) {
            $token = $this->tokenStorage->getToken();
            $request = $this->requestStack->getCurrentRequest();
            $requestData = json_decode($request->getContent());
            $entity->setDesiredPrice($entity->createDesiredPrice());
            $parsedUrl = parse_url($requestData->url);

            if ($token && isset($parsedUrl['host'])) {
                $host = $this->createHost($parsedUrl['host']);
                $product = $this->createProduct($args->getEntityManager(), $host, $requestData);
                $user = $token->getUser();

                if (!$user && !$product) {
                    throw new BadRequestHttpException('Cannot create product');
                }

                $productWatcher = $this->productWatcherRepository->findOneByUserAndProduct($user->getId(), $product->getId());

                if ($productWatcher) {
                    throw new BadRequestHttpException('Product already exist');
                }

                $entity->setUser($user);
                $entity->setProduct($product);
            } else {
                throw new BadRequestHttpException('Url is not valid');
            }
        }
    }

    private function createHost(string $host): Host
    {
        $entity = $this->hostRepository->findOneByName($host);
        if (!$entity) {
            $entity = $this->hostRepository->createByName($host);
        }

        return $entity;
    }

    private function createProduct(EntityManager $entityManager, Host $host, $requestData): Product
    {
        $product = $this->productRepository->findOneByUrl($requestData->url);

        if (!$product) {
            $product = $this->productRepository->fillInData($requestData);
            $product->setHost($host);
            $entityManager->persist($product);
            $entityManager->flush();
        }

        return $product;
    }
}
