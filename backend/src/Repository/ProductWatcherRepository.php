<?php

namespace App\Repository;

use App\Entity\Host;
use App\Entity\Price;
use App\Entity\Product;
use App\Entity\ProductWatcher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Host|null find($id, $lockMode = null, $lockVersion = null)
 * @method Host|null findOneBy(array $criteria, array $orderBy = null)
 * @method Host[]    findAll()
 * @method Host[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductWatcherRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductWatcher::class);
    }

    // /**
    //  * @return Host[] Returns an array of Host objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

//    public function findOneByName($value): ?Host
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.name = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }

    public function getNewSuccessful(): array
    {
        return $this->findBy(['status' => ProductWatcher::STATUS_NEW_SUCCESS]);
    }

    public function updateStatusIfPriceDesired(Price $price, Product $product)
    {
        return $this->createQueryBuilder('pw')
            ->update()
            ->set('pw.status', ProductWatcher::STATUS_NEW_SUCCESS)
            ->set('pw.successDate', time())
            ->where('pw.product = :product_id AND pw.status != :success_status AND pw.status != :success_new_status AND pw.deletedAt IS NULL')
            ->andWhere('pw.desiredPrice >= :price')
            ->setParameters([
                'product_id' => $product->getId(),
                'success_status' => ProductWatcher::STATUS_SUCCESS,
                'success_new_status' => ProductWatcher::STATUS_NEW_SUCCESS,
                'price' => $price->getPrice()
            ])
            ->getQuery()
            ->execute();
    }

    public function findOneByUserAndProduct($userId, $productId): ?ProductWatcher
    {
        return $this->findOneBy(['user' => $userId, 'product' => $productId]);
    }

    public function fillInData($data): ProductWatcher
    {
        $productWatcher = new ProductWatcher();
        $productWatcher->setPercent($data->percent);
        $productWatcher->setTitle($data->title);
        $productWatcher->setStartPrice($data->currentPrice);
        return $productWatcher;
    }
}
