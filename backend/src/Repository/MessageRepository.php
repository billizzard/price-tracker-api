<?php

namespace App\Repository;

use App\Entity\Message;
use App\Entity\ProductWatcher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    private $twig;

    public function __construct(RegistryInterface $registry, \Twig_Environment $twig)
    {
        parent::__construct($registry, Message::class);
        $this->twig = $twig;
    }

    public function createSuccessMessageByProductWatcher(ProductWatcher $productWatcher)
    {
        $message = new Message();
        $user = $productWatcher->getUser();

        $messageContent = $this->twig->render('/messages/successProductWatcherPrice.html.twig', [
            'title' => 'The price of the goods reached the desired price.',
            'productWatcherTitle' => $productWatcher->getTitle(),
            'productWatcherDesiredPrice' => $productWatcher->getDesiredPrice()
        ]);

        $message->setUser($user);
        $message->setTitle('Achieved the desired price');
        $message->setMessage($messageContent);
        $message->setType(Message::TYPE_ACHIEVED_DESIRED_PRICE);

        $em = $this->getEntityManager();
        $em->persist($message);
        $em->flush();
    }
}
